/**
 * First project for AP Computer Science. Outputs a simple message to the console.
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @revised Sep 07 2017
 */

public class MyFirstProject {
	 
	public static void main(String[] args) {
		System.out.println("Josh Thibeault is probably the author of this program. Javadocs would be nice...");
	}
	
 }