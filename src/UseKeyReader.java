/**
 * Introductory class to listeners and Jva imports
 * 
 * @author 18thibeaultj
 * @block Blue 3, Hunt
 * @revised Sep 13 2017
 */

import java.util.Scanner;

public class UseKeyReader {

	private static Scanner keyReader;  //Used in a static class, also should be a static method. Only needed locally

	public static void main(String[] args) {
		keyReader = new Scanner(System.in);
		int userAge;
		double lunchCost;
		String userName;
		
		System.out.print("Enter your first name: ");
		userName = keyReader.nextLine();
		
		System.out.print("Enter your age: ");
		userAge = keyReader.nextInt();
		
		System.out.print("Enter the cost of yesterday's lunch: ");
		lunchCost = keyReader.nextDouble();
		
		System.out.println("Thank you, " + userName + ", you are " + userAge + " years old, and spent $" + lunchCost + " on lunch yesterday.");
	}

}
